# frozen_string_literal: true

module EpicTreeSorting
  extend ActiveSupport::Concern
  include FromUnion
  include RelativePositioning

  class_methods do
    def relative_positioning_query_base(object)
      from_union([
        EpicIssue.select("id, relative_position, epic_id as parent_id, epic_id, 'epic_issue' as object_type").in_epic(object.parent_ids),
        Epic.select("id, relative_position, parent_id, parent_id as epic_id, 'epic' as object_type").where(parent_id: object.parent_ids)
      ])
    end

    def relative_positioning_parent_column
      :epic_id
    end
  end

  included do
    extend ::Gitlab::Utils::Override

    override :move_sequence
    def move_sequence(start_pos, end_pos, delta, include_self = false)
      items_to_update = scoped_items
        .select(:id, :object_type)
        .where('relative_position BETWEEN ? AND ?', start_pos, end_pos)

      unless include_self
        items_to_update = relative_siblings(items_to_update)
      end

      items_to_update.group_by { |item| item.object_type }.each do |type, group_items|
        ids = group_items.map(&:id)
        items = type.camelcase.constantize.where(id: ids).select(:id)
        items.update_all("relative_position = relative_position + #{delta}")
      end
    end

    private

    override :relative_siblings
    def relative_siblings(relation = scoped_items)
      relation.where.not('object_type = ? AND id = ?', self.class.table_name.singularize, self.id)
    end
  end
end
